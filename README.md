# Prometheus Operator Catalog 

Prometheus Operator manifests, and terraform implementation, 
that can be used to treat the Operator like an appliance in your cluster.

## Installation

To install the Prometheus Operator in your cluster, 
simply add a new module to your terraform repo

```hcl-terraform
module "prometheus-operator" {
  source = "git::https://gitlab.com/chris-sandbox/terraform-prometheus-operator-kubernetes.git"
}
``` 

## Overriding configurations

The module allows overriding some settings, to suite the needs of your cluster

```hcl-terraform
module "prometheus-operator" {
  source = "git::https://gitlab.com/chris-sandbox/terraform-prometheus-operator-kubernetes.git"

  /**
   Override the namespace where the operator is installed.
   All namespaced resources within the module, will be installed within this namespace
  */
  namespace = "testing-operator"
  
  /**
   Set some common labels, to make it easier to search for parts belonging to the Operator
   These will be merged with some common labels already defined
   {
       component: "prometheus-operator",
       managed_by: "terraform",
   }
  */
  labels = {
    operator: "prometheus"
  }
}
```