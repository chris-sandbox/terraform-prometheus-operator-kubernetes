variable "namespace" {
  type = string
  default = "prometheus-operator"
}

variable "labels" {
  type = map(string)
  default = {}
}