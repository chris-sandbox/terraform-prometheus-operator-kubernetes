provider "kustomization" {}

resource "kustomization_resource" "prometheus-operator" {
  for_each = data.kustomization_overlay.prometheus-operator.ids

  manifest = data.kustomization_overlay.prometheus-operator.manifests[each.value]
}

data "kustomization_overlay" "prometheus-operator" {
  namespace = var.namespace

  common_labels = merge({
    component: "prometheus-operator"
    managed_by: "terraform"
  }, var.labels)

  resources = [
    "${path.module}/manifests",
  ]
}
